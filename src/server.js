// src/server.js
const express = require("express")
const converter = require("./converter")

const app = express()
const port = 3000

app.get('/', (req, res) => res.send("Hello"))

app.get('/rgb-to-hex', (req, res) => {
    // const red = parseInt(req.query.red, 10)
    // const green = parseInt(req.query.green, 10)
    // const blue = parseInt(req.query.blue, 10)
    // const hex = converter.rgbToHex(red,green,blue)
    // res.send(hex)

    res.send(
        converter.rgbToHex(
            parseInt(req.query.red, 10),
            parseInt(req.query.green, 10),
            parseInt(req.query.blue, 10)
            )
        )
})

if (process.env.NODE_ENV === "test"){
    module.exports = app
} else {
    app.listen(port, () => console.log(`Server: localhost:${port}`))
}

